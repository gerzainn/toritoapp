package dev.com.toreritoapp;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.nfc.Tag;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.android.billingclient.api.SkuDetailsResponseListener;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Pago extends AppCompatActivity implements PurchasesUpdatedListener {

    private static final String TAG = Pago.class.getSimpleName();
    private BillingClient mBillingClient;
    private View mPwgButton;
    private Map<String, SkuDetails> mSkuDetailsMap = new HashMap<>();
    List<String> productos = new ArrayList<>();




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pago);

        setUpBillig();
        mPwgButton = findViewById(R.id.btn_pagar);

        mPwgButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                /*BillingFlowParams flowParams = BillingFlowParams.newBuilder()
                        .setSkuDetails(mSkuDetailsMap.get("amp_01"))
                        .build();

                BillingResult responseCode = mBillingClient.launchBillingFlow(Pago.this, flowParams);
                int responsecode = responseCode.getResponseCode();

                Log.e(TAG, String.valueOf(responsecode));*/

                getProductos();
            }
        });



    }


    public void  setUpBillig()
    {
        mBillingClient = BillingClient
                .newBuilder(Pago.this)
                .setListener(this)
                .enablePendingPurchases()
                .build();
        mBillingClient.startConnection(new BillingClientStateListener()
        {

            @Override
            public void onBillingSetupFinished(BillingResult billingResult) {
                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK)
                {
                    Log.d(TAG, "RespuestaOK");

                }
                else

                    {
                    Log.e(TAG , "error" + billingResult.getResponseCode());
                }
            }

            @Override
            public void onBillingServiceDisconnected() {

                Log.e(TAG ,"Desconectado");
            }
        });

    }

    public void getProductos()
    {
        productos.add("amp_01");

        if(mBillingClient.isReady())
        {

            SkuDetailsParams.Builder params = SkuDetailsParams
                    .newBuilder();
            params.setSkusList(productos)
                    .setType(BillingClient.SkuType.INAPP)
                    .build();
            mBillingClient.querySkuDetailsAsync(params.build(),
                    new SkuDetailsResponseListener()
                    {
                        @Override
                        public void onSkuDetailsResponse(BillingResult billingResult, List<SkuDetails> list) {
                            if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK )
                            {
                                Log.d("TAG", "Sucees " + billingResult);

                                for (SkuDetails skuDetails : list)
                                {
                                    String sku = skuDetails.getSku();
                                    String price = skuDetails.getPrice();

                                    mSkuDetailsMap.put(skuDetails.getSku(), skuDetails);
                                    Log.d(TAG, sku + "" + price);

                                }

                            }
                        }
                    });

        }
        else


            {
            Log.e(TAG,"Ready-false");
        }


    }


    @Override
    public void onPurchasesUpdated(BillingResult billingResult, @Nullable List<Purchase> list)
    {
        if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK

                && list != null)
        {
            for (Purchase purchase : list) {

            }
        }
    }

}

