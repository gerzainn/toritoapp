package dev.com.toreritoapp;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

public class ActionBottomDialogCompleteGoogleAccount extends BottomSheetDialogFragment implements View.OnClickListener
{
    public static final String TAG = "ActionBottomDialog";

    private ActionBottomDialogCompleteGoogleAccount.ItemClickListener mListener;
    public static  ActionBottomDialogCompleteGoogleAccount newInstance() {
        return new ActionBottomDialogCompleteGoogleAccount();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.completegoogleaccount, container, false);
    }

    @Override public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.findViewById(R.id.btnEntendido).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Button btnintentar = (Button) view;
        mListener.onItemClick();
        dismiss();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ActionBottomDialogCompleteGoogleAccount.ItemClickListener) {
            mListener = (ActionBottomDialogCompleteGoogleAccount.ItemClickListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement ItemClickListener");
        }
    }
    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface ItemClickListener {
        void onItemClick();
    }
}


