package dev.com.toreritoapp;

import android.content.Intent;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.AppCompatCheckBox;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import dev.com.toreritoapp.model.User;

public class Registro extends AppCompatActivity {
    private DatabaseReference mFirebaseDatabase;
    private FirebaseDatabase mFirebaseInstance;
    private FirebaseAuth firebaseAuth;
    private TextInputEditText nombre,email,password, lastname, phone;
    private TextInputLayout inputLayoutnombre, inputLayoutapellido, inputLayoutemail, inputLayoutpassword, inputLayoutphone;
    private Button btnregistro;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        mFirebaseInstance = FirebaseDatabase.getInstance();
        mFirebaseDatabase = mFirebaseInstance.getReference("users");
        firebaseAuth      = FirebaseAuth.getInstance();


        AppCompatCheckBox checkterms;
        nombre= findViewById(R.id.edt_nombre);
        email = findViewById(R.id.edit_email);
        password= findViewById(R.id.edt_pasword);
        lastname = findViewById(R.id.edt_apellidos);
        phone = findViewById(R.id.edt_telefono);
        btnregistro= findViewById(R.id.btn_registro);
        inputLayoutnombre= findViewById(R.id.textinputnombre);
        inputLayoutapellido= findViewById(R.id.textinputapellidos);
        inputLayoutemail= findViewById(R.id.textinputemail);
        inputLayoutphone= findViewById(R.id.textinputphone);
        checkterms=findViewById(R.id.check_terms);



        nombre.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() > 0)
                {
                    Log.d("Registro",s.toString());

                }



            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length() > 0)
                {
                   Log.d("Registro", s.toString());
                }
            }
        });

        password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() > 0)
                {
                    btnregistro.setBackgroundResource(R.drawable.button_background_red);
                }
                else
                {
                    btnregistro.setBackgroundResource(R.drawable.btn_background_gray);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        checkterms.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(buttonView.isChecked())
                {
                    btnregistro.setBackgroundResource(R.drawable.button_background_red);
                }
                else
                {
                    btnregistro.setBackgroundResource(R.drawable.btn_background_gray);
                }
            }
        });

        btnregistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createUser();
            }
        });
    }

    public void createUser()
    {

        if(!validar()) {
            return;
        }
            String name = nombre.getText().toString().trim();
            String useremail= email.getText().toString().trim();
            String pass = password.getText().toString().trim();
            String apellidos = lastname.getText().toString().trim();
            String telefono = phone.getText().toString().trim();

            firebaseAuth.createUserWithEmailAndPassword(useremail,pass)
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e)
                        {
                            Toast.makeText(getApplicationContext(),e.getMessage(),Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if(task.isSuccessful())
                            {
                                Log.d("Registro","correcto");
                                Toast.makeText(getApplicationContext(),"usuario agregado",Toast.LENGTH_SHORT).show();
                                onAuthSucess(Objects.requireNonNull(task.getResult().getUser()));
                                finish();
                            }
                            else
                            {
                                Log.e("Registro","error ");
                            }
                        }

                    });

    }

    public boolean validar()
    {
        boolean valid = true;
        String  name  = nombre.getText().toString();
        if(TextUtils.isEmpty(name)) {

            valid = false;
            inputLayoutnombre.setError(getString(R.string.validarnombre));
            btnregistro.setBackgroundResource(R.drawable.btn_background_gray);
            btnregistro.setEnabled(false);
        }
        else
        {
            inputLayoutnombre.setError(null);
            btnregistro.setEnabled(true);
            btnregistro.setBackgroundResource(R.drawable.button_background_red);
        }
        String apellidos =  lastname.getText().toString();
        if(TextUtils.isEmpty(apellidos)) {
            valid = false;
            inputLayoutapellido.setError(getString(R.string.validarapellido));
            btnregistro.setBackgroundResource(R.drawable.btn_background_gray);
            btnregistro.setEnabled(false);

        }
        else {
            inputLayoutnombre.setError(null);
            btnregistro.setEnabled(true);
            btnregistro.setBackgroundResource(R.drawable.button_background_red);
        }
        String correo= email.getText().toString().trim();
        if(TextUtils.isEmpty(correo)) {
            valid = false;
            inputLayoutemail.setError(getString(R.string.validaremail));
            btnregistro.setBackgroundResource(R.drawable.btn_background_gray);
            btnregistro.setEnabled(false);

        }
        else
        {
            inputLayoutemail.setError(null);
            btnregistro.setEnabled(true);
            btnregistro.setBackgroundResource(R.drawable.button_background_red);
        }

          return valid;
    }

    public   void onAuthSucess(FirebaseUser user)
    {
        String username = nombre.getText().toString();
        String use_id = user.getUid();
        String useremail= email.getText().toString().trim();
        String pass = password.getText().toString().trim();
        String apellidos = lastname.getText().toString().trim();
        String telefono = phone.getText().toString().trim();

        writeUser(use_id,username,useremail,apellidos,pass,telefono);
}

    private String usernameFromEmail(String email)
    {
        if (email.contains("@")) {
            return email.split("@")[0];
        } else {
            return email;
        }
    }
    public   void writeUser(String userid, String name, String email,String lastname,String pass,String phone)
    {
        User user = new User(name,email,lastname,pass,phone);
        Map<String, Object> postValues = user.tomap();

        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/" + userid, postValues);

        mFirebaseDatabase.updateChildren(childUpdates);
    }
}
