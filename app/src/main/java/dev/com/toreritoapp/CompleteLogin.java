package dev.com.toreritoapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

import dev.com.toreritoapp.model.User;

public class CompleteLogin extends AppCompatActivity implements  GoogleApiClient.OnConnectionFailedListener , ActionBottomDialogCompleteGoogleAccount.ItemClickListener {

    private DatabaseReference mFirebaseDatabase;
    private  DatabaseReference databaseReferencesave;
    private FirebaseDatabase mFirebaseInstance;
    private GoogleApiClient googleApiClient;
    private String      googleid;
    private TextInputEditText email,nombre,lastname, phone;
    private TextInputLayout inputLayoutnombre, inputLayoutapellido, inputLayoutphone;
    private Button  btnguardar;
    private static final String TAG = CompleteLogin.class.getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.completelogingoogle);

        mFirebaseInstance = FirebaseDatabase.getInstance();
        databaseReferencesave= mFirebaseInstance.getReference("users");

        email                   = findViewById(R.id.edt_emailgoogle);
        nombre                  = findViewById(R.id.edt_nombregoogle);
        lastname                = findViewById(R.id.edt_apellidosgoogle);
        phone                   = findViewById(R.id.edt_telefonogoolge);
        inputLayoutnombre       = findViewById(R.id.textinputgooglename);
        inputLayoutapellido     = findViewById(R.id.textinputapellidosgoogle);
        inputLayoutphone        = findViewById(R.id.textInputLayouttelefonogoogle);
        btnguardar              = findViewById(R.id.btnguardardatos);

        btnguardar.setBackgroundResource(R.drawable.btn_background_gray);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        btnguardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               if(!validarRegistoGoogle())
               {
                   return;
               }
                guardatos();
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(googleApiClient);
        if (opr.isDone()) {
            GoogleSignInResult result = opr.get();
            handleResult(result);
        } else {
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(@NonNull GoogleSignInResult googleSignInResult) {
                    handleResult(googleSignInResult);
                }
            });
        }
    }

    private void handleResult (GoogleSignInResult result ) {
        if (result.isSuccess()) {
            GoogleSignInAccount account = result.getSignInAccount();

            if (account != null) {

                final String personEmail = account.getEmail();
                email.setText(personEmail);
                googleid           = account.getId();

                mFirebaseDatabase = mFirebaseInstance.getReference("users/" + googleid);

                mFirebaseDatabase.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        User usuario = dataSnapshot.getValue(User.class);
                        if(usuario != null)
                        {
                            String email = usuario.getEmail();

                            if(email.equals(personEmail))
                            {
                                Intent intent = new Intent(getApplicationContext(), Amparo.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                finish();
                            }
                            else
                            {
                              showBottomSheet();
                              Log.d(TAG ," completarcuenta");
                            }
                        }
                        else
                        {
                            showBottomSheet();
                        }

                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.e(TAG , databaseError.getMessage());
                    }
                });
            }
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    public void showBottomSheet() {
        ActionBottomDialogCompleteGoogleAccount addPhotoBottomDialogFragment =
                ActionBottomDialogCompleteGoogleAccount.newInstance();
        addPhotoBottomDialogFragment.show(getSupportFragmentManager(),
                ActionBottomDialogCompleteGoogleAccount.TAG);
    }

    public boolean validarRegistoGoogle()
    {
        boolean valid = true;
        String  name  = nombre.getText().toString();
        if(TextUtils.isEmpty(name)) {

            valid = false;
            inputLayoutnombre.setError(getString(R.string.validarnombre));
            btnguardar.setBackgroundResource(R.drawable.btn_background_gray);
        }
        else
        {
            inputLayoutnombre.setError(null);
            btnguardar.setEnabled(true);
            btnguardar.setBackgroundResource(R.drawable.button_background_red);
        }
        String apellido =  lastname.getText().toString();
        if(TextUtils.isEmpty(apellido)) {
            valid = false;
            inputLayoutapellido.setError(getString(R.string.validarapellido));
            btnguardar.setBackgroundResource(R.drawable.btn_background_gray);

        }
        else {
            inputLayoutnombre.setError(null);
            btnguardar.setBackgroundResource(R.drawable.button_background_red);
        }
        String telefono= phone.getText().toString().trim();
        if(TextUtils.isEmpty(telefono)) {
            valid = false;
            inputLayoutphone.setError(getString(R.string.validartelefono));
            btnguardar.setBackgroundResource(R.drawable.btn_background_gray);
        }
        else
        {
            inputLayoutphone.setError(null);
            btnguardar.setBackgroundResource(R.drawable.button_background_red);
        }
        return valid;
    }

    public  void guardatos()
    {
        String  name        = nombre.getText().toString();
        String apellido     =  lastname.getText().toString();
        String correo       = email.getText().toString().trim();
        String telefono     = phone.getText().toString().trim();
        Map<String, Object> userMap = new HashMap<>();
        userMap.put(googleid,new User(name,apellido,correo,telefono));
        databaseReferencesave.updateChildren(userMap);
    }


    @Override
    public void onItemClick() {

    }
}
