package dev.com.toreritoapp;

import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class Account extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener , ActionBottomDialogFragmentLogin.ItemClickListener {

    private GoogleApiClient googleApiClient;
    private  static  final int SIGN_IN_CODE = 777;
    private static final String TAG = Account.class.getSimpleName();
    private FirebaseAuth mauth;
    private TextInputEditText user;
    private TextInputEditText password;
    private Button iniciarsesion;
    private TextInputLayout textInputLayoutuser, textInputLayoutpassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complete_login);
        SignInButton btnSignIn = findViewById(R.id.btn_googlelogin);
        TextView txtRegistro = findViewById(R.id.tv_registrar);
        TextView txtRecoverpass = findViewById(R.id.tv_pass);
        ImageButton close       = findViewById(R.id.toolbarlogin);
        user = findViewById(R.id.edt_userlogin);
        password= findViewById(R.id.edit_passwordlogin);
        iniciarsesion = findViewById(R.id.btnloginauth);
        textInputLayoutuser = findViewById(R.id.textinputuser);
        textInputLayoutpassword=findViewById(R.id.textinputpassauth);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        googleApiClient=new GoogleApiClient.Builder(this)
                .enableAutoManage(this,this)
                .addApi(Auth.GOOGLE_SIGN_IN_API,gso)
                .build();


        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 IniciarSesion();
            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        txtRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent registro = new Intent(getApplicationContext(),Registro.class);
                startActivity(registro);
            }
        });

        txtRecoverpass.setOnClickListener(

                new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent recoverpass = new Intent(getApplicationContext(),RecoverPassword.class);
                startActivity(recoverpass);
                    }
        });

        mauth = FirebaseAuth.getInstance();

        iniciarsesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IniciarSesionFirebase();
            }
        });

        if(mauth.getCurrentUser()!= null)
        {
            startActivity(new Intent( getApplicationContext(), Config.class));
        }

        changeText();

    }

    public void changeText()
    {
        if((Build.VERSION.SDK_INT <= 22))
        {
            iniciarsesion.setText(getString(R.string.Entrar));
        }
    }
    @Override
    protected void onStart()
    {
        super.onStart();
    }

    private void IniciarSesion()
    {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        startActivityForResult(signInIntent, SIGN_IN_CODE);
    }
    private  void IniciarSesionFirebase()
    {
        if(!validarForm())
        {
            return;
        }
        String email = user.getText().toString();
        String pass = password.getText().toString();

        mauth.signInWithEmailAndPassword(email,pass) .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful())
                {
                    Log.d(TAG, "inicio sesion ");
                    startActivity(new Intent( getApplicationContext(), Amparo.class));
                }
                else
                {
                    Log.w(TAG,"fallo", task.getException());

                    //loginerror.setVisibility(View.VISIBLE);
                    showBottomSheet();
                }
                    }
                });
    }

    public void showBottomSheet() {
        ActionBottomDialogFragmentLogin addPhotoBottomDialogFragment =
                ActionBottomDialogFragmentLogin.newInstance();
        addPhotoBottomDialogFragment.show(getSupportFragmentManager(),
                ActionBottomDialogFragmentLogin.TAG);
    }

    private  boolean validarForm()
    {
        boolean valid = true;
        String email = user.getText().toString();
        if(TextUtils.isEmpty(email))
        {
            valid=false;
            textInputLayoutuser.setError(getString(R.string.validarusuario));
            iniciarsesion.setBackgroundResource(R.drawable.btn_background_gray);
        }else
        {
            iniciarsesion.setBackgroundResource(R.drawable.button_background_red);
        }
        String pass = password.getText().toString();
        if(TextUtils.isEmpty(pass))
        {
            valid=false;
            iniciarsesion.setBackgroundResource(R.drawable.btn_background_gray);
            textInputLayoutpassword.setError(getString(R.string.validarpassword));
        }
        else
            {
                iniciarsesion.setBackgroundResource(R.drawable.button_background_red);
            }
        return  valid;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==SIGN_IN_CODE)
        {
            GoogleSignInResult result=Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }
    private  void handleSignInResult(GoogleSignInResult result) {

        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        Log.d(TAG, "Codigo"+ " "+ result.getStatus());
        if (result.isSuccess())
        {
            GoogleSignInAccount account = result.getSignInAccount();
            if (account != null) {
                String personName = account.getDisplayName();
                String personGivenName = account.getGivenName();
                String personFamilyName = account.getFamilyName();
                String personEmail = account.getEmail();
                String personId = account.getId();
                goLogInScreen();

            }
        }
        else
            {
            Log.e(TAG, "Error al iniciar sesion");
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e(TAG,"Fallo conexión");
    }

    private void goLogInScreen()
    {
        Intent intent = new Intent(this, CompleteLogin.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void onItemClick() {

    }
}
