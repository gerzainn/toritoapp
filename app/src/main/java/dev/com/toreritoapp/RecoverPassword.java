package dev.com.toreritoapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;

public class RecoverPassword extends AppCompatActivity {

    private TextInputLayout inputLayoutemail;
    private TextInputEditText email;
    private Button enviarpassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recover_password);
        Toolbar toolbar = findViewById(R.id.tolbarrecoverpassword);
        setSupportActionBar(toolbar);
        inputLayoutemail = findViewById(R.id.contenedor_recuperarpass);
        email = findViewById(R.id.edtrecoverpass);
        enviarpassword = findViewById(R.id.btnrecuperarpass);

        email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(charSequence.equals("^[A-Z0-9+_.-]+@[A-Z0-9.-]+$"))
                {
                    enviarpassword.setBackgroundResource(R.drawable.button_background_red);
                }
                else
                {
                    enviarpassword.setBackgroundResource(R.drawable.btn_background_gray);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        enviarpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!validar()) {
                    return;
                }
                FirebaseAuth auth = FirebaseAuth.getInstance();
                String emailAddress = email.getText().toString();

                auth.sendPasswordResetEmail(emailAddress)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    Log.d("EmailRecover", "Email sent.");
                                }

                                else
                                {
                                    Log.e("EmailRecover", "Error al enviar email");
                                }
                            }
                        });

            }
        });

    }


    public boolean validar() {
        boolean valid = true;
        String correo = email.getText().toString();
        if (TextUtils.isEmpty(correo)) {

            valid = false;
            inputLayoutemail.setError(getString(R.string.validaremail));
            enviarpassword.setBackgroundResource(R.drawable.btn_background_gray);
        } else {
            inputLayoutemail.setError(null);
            enviarpassword.setBackgroundResource(R.drawable.button_background_red);
        }
        return  valid;
    }
}
