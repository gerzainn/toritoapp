package dev.com.toreritoapp.model;

import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.Map;

public class User
{
    private String firstname;
    private String lastname;
    private String middelname;
    private String password;
    private String phone;
    private String email;

    public String getId() {
        return uid;
    }

    public void setId(String id) {
        this.uid = id;
    }

    private String uid;

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getMiddelname() {
        return middelname;
    }

    public void setMiddelname(String middelname) {
        this.middelname = middelname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public User(String firstname, String lastname, String pass, String tel , String email) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.password = pass;
        this.phone = tel;
        this.email = email;
    }


    public User(String firstname,String  lastname, String email , String telefono)
    {
        this.firstname  = firstname;
        this.lastname   = lastname;
        this.email      = email;
        this.phone      = telefono;
    }
    public User()
    {

    }

    @Exclude
    public Map<String,Object> tomap()
    {
        HashMap<String, Object> result = new HashMap<>();
        result.put("firstname", firstname);
        result.put("email",email);
        result.put("lastname",lastname);
        result.put("password",password);
        result.put("phone",phone);

        return  result;
    }


}
