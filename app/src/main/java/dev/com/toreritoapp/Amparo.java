package dev.com.toreritoapp;

import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.ConsumeParams;
import com.android.billingclient.api.ConsumeResponseListener;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.android.billingclient.api.SkuDetailsResponseListener;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import de.hdodenhof.circleimageview.CircleImageView;

public class Amparo extends AppCompatActivity implements  GoogleApiClient.OnConnectionFailedListener , PurchasesUpdatedListener {

    private GoogleApiClient googleApiClient;
    private GoogleSignInClient googleSignInClient;
    private TextView name;
    private TextView email;
    private FirebaseAuth firebaseAuth;
    private CircleImageView profile;
    private BillingClient mBillingClient;
    private Button btn_amparoamigo;
    private  int codigorespuesta;
    private  int tamlista;
    private  String idgoogle;
    private  String orderId;
    private String nombre_google;
    private DatabaseReference mFirebaseDatabase;
    private FirebaseDatabase mFirebaseInstance;

    private  final static  String Tag = Amparo.class.getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile);

        FirebaseStorage storage = FirebaseStorage.getInstance();
        btn_amparoamigo = findViewById(R.id.btnAmaparo_amigo);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        firebaseAuth= FirebaseAuth.getInstance();
        mFirebaseInstance = FirebaseDatabase.getInstance();
        mFirebaseDatabase = mFirebaseInstance.getReference("users");




        btn_amparoamigo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


        setUpBillig();

    }

    @Override
    protected void onStart()
    {
        super.onStart();

        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(googleApiClient);
        if (opr.isDone()) {
            GoogleSignInResult result = opr.get();
            handleResult(result);
        } else {
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(@NonNull GoogleSignInResult googleSignInResult) {
                    handleResult(googleSignInResult);
                }
            });
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    private void handleResult (GoogleSignInResult result )
    {
        if(result.isSuccess())
        {
            GoogleSignInAccount account = result.getSignInAccount();

            if(account!=null)
            {

                Log.d(Tag, "Exito al cargar el perfil");
                nombre_google= account.getDisplayName();
                idgoogle = account.getId();
                String personName = account.getDisplayName();
                String personGivenName = account.getGivenName();
                String personFamilyName = account.getFamilyName();
                String personEmail = account.getEmail();

            }
        }
    }

    public void logOut() {
        Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(@NonNull Status status) {
                if (status.isSuccess())
                {

                    goLogInScreen();
                } else
                {
                    Toast.makeText(getApplicationContext(), "Eror el cerrar sesión", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void goLogInScreen()
    {
        Intent intent = new Intent(this, Login.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_amparo, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.amparosalir :
                firebaseAuth.signOut();
                logOut();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }



    public void PagoAmparo(View view)
    {
        List<String> products = new ArrayList<>();
        products.add("amp_01");

        if(mBillingClient.isReady())
        {
            SkuDetailsParams.Builder params = SkuDetailsParams
                    .newBuilder();
            params.setSkusList(products)
                    .setType(BillingClient.SkuType.INAPP)
                    .build();
            mBillingClient.querySkuDetailsAsync(params.build(), new SkuDetailsResponseListener() {
                @Override
                public void onSkuDetailsResponse(BillingResult billingResult, List<SkuDetails> list) {

                    if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK  )
                    {
                         codigorespuesta = billingResult.getResponseCode();
                         tamlista= list.size();
                        Log.d(Tag, "Sucees " +codigorespuesta +" "+ "Tam"+ tamlista);



                        for (SkuDetails skuDetails : list)
                        {
                            String sku = skuDetails.getSku();
                            String price = skuDetails.getPrice();

                            BillingFlowParams flowParams = BillingFlowParams.newBuilder()
                                    .setSkuDetails(skuDetails)
                                    .build();
                            BillingResult responsecode = mBillingClient.launchBillingFlow(Amparo.this,flowParams);
                            int response = responsecode.getResponseCode();

                            Log.d(Tag , "precio" + price + " sku"+ " " + sku);
                            Log.d(Tag, "codigoBilligResult"+ " " + response);
                    }

                    }

                    else
                    {
                        Log.e(Tag,  String.valueOf(codigorespuesta));
                    }
                }
            });

        }
        else
        {
            Toast.makeText(getApplicationContext(),"BillingNotReady",Toast.LENGTH_LONG).show();
        }


    }


    public void setUpBillig()
    {
        mBillingClient = BillingClient
                .newBuilder(Amparo.this)
                .setListener(this)
                .enablePendingPurchases()
                .build();
        mBillingClient.startConnection(new BillingClientStateListener()
        {

            @Override
            public void onBillingSetupFinished(BillingResult billingResult) {
                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK)
                {
                    Log.d(Tag, "RespuestaOK");

                }
                else

                {
                    Log.e(Tag , "error" + billingResult.getResponseCode());
                }
            }

            @Override
            public void onBillingServiceDisconnected() {

                Log.e(Tag ,"Desconectado");
            }
        });
    }

    public void  handlePurchase(Purchase purchase)
    {
        if(purchase!=null)
        {
            ConsumeParams consumeParams= ConsumeParams.newBuilder()
                    .setPurchaseToken(purchase.getPurchaseToken())
                    .build();

            ConsumeResponseListener consumeResponseListener= new ConsumeResponseListener() {
                @Override
                public void onConsumeResponse(BillingResult billingResult, String s) {
                    if(billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK && s!= null)
                    {
                        Log.d(Tag, "onConsumeResponseOk");
                    }
                }
            };
            mBillingClient.consumeAsync(consumeParams,consumeResponseListener);

        }
    }

    @Override
    public void onPurchasesUpdated(BillingResult billingResult, @Nullable List<Purchase> list)
    {

        if(billingResult.getResponseCode() ==  BillingClient.BillingResponseCode.OK
         && list != null)
        {
            for (Purchase purchase : list)
            {
                handlePurchase(purchase);
                orderId= purchase.getOrderId();
            }

            createPdf();
        }
        else if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.USER_CANCELED)
        {
            Toast.makeText(getApplicationContext(), "Se cancelo la compra", Toast.LENGTH_SHORT).show();
        }
        else if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.ITEM_ALREADY_OWNED)
        {
            Toast.makeText(getApplicationContext(), "El producto ya se compro anteriormente ", Toast.LENGTH_SHORT).show();

        }
        else if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.ERROR)
        {
            Toast.makeText(getApplicationContext(),"developer error",Toast.LENGTH_SHORT).show();
        }
    }


    public void createPdf()
    {
        String directorio = "/Torerito/";
        String pdfextension = ".pdf";
        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference rootreference = storage.getReference("Users");
        DatabaseReference mFirebaseDatabase;
        FirebaseDatabase mFirebaseInstance;

        mFirebaseInstance = FirebaseDatabase.getInstance();
        mFirebaseDatabase = mFirebaseInstance.getReference("users/"+ idgoogle+"/amparo/");
        StorageReference userRef;



        String directory_path = Environment.getExternalStorageDirectory().getAbsolutePath()+ "/torerito/"+ idgoogle;
        File file = new File(directory_path);
        if(!file.exists())
        {
            file.mkdir();

        }
        String targetpdf =  directory_path+"/"+idgoogle+".pdf";
        File filePath = new File(targetpdf);
        try
        {

            SimpleDateFormat format = new SimpleDateFormat("dd 'de' MMMM 'del' yyyy", Locale.getDefault());
            String datestring = format.format(new Date());

            Font boldfont = new Font(Font.FontFamily.TIMES_ROMAN,12,Font.BOLD);
            Document document= new Document();
            PdfWriter.getInstance(document,new FileOutputStream(targetpdf));
            document.open();

            Paragraph paragraph = new Paragraph("DEMANDA URGENTE CONFORME AL ARTÍCULO 15 DE LA LEY DE AMPARO AMPARO INDIRECTO");
            paragraph.setAlignment(Element.ALIGN_LEFT);
            document.add(paragraph);

            paragraph= new Paragraph("AMPARO INDIRECTO",boldfont);
            paragraph.setAlignment(Element.ALIGN_JUSTIFIED);
            paragraph.setSpacingAfter(15f);
            document.add(paragraph);

            paragraph = new Paragraph("C. Juez de Distrito en Materia Administrativa en la Ciudad de México en Turno.");
            paragraph.setAlignment(Element.ALIGN_JUSTIFIED);
            paragraph.setSpacingAfter(15f);
            document.add(paragraph);

            paragraph = new Paragraph("Ciudad de Mexico a."+""+ datestring);
            document.add(paragraph);
            paragraph.setSpacingAfter(15f);
            paragraph.setAlignment(Element.ALIGN_JUSTIFIED);

            paragraph = new Paragraph(nombre_google+ " "+ "en nombre y representación del quejoso " + nombre_google + " y señalando como domicilio para el efecto de oír y recibir notificaciones las listas publicadas en ese Juzgado Federal, a Usted C. Juez atentamente manifiesto lo siguiente:");
            document.add(paragraph);
            paragraph.setAlignment(Element.ALIGN_JUSTIFIED);

            paragraph = new Paragraph("Que por medio del presente escrito vengo en nombre y representación del hoy quejoso ya mencionado, a solicitar el " + "AMPARO Y PROTECCIÓN DE LA JUSTICIA DE LA UNIÓN"+ " en contra de las autoridades y actos que a continuación señalo, por lo que ajustándome al artículo 108 y 15 de la Ley de Amparo que establece lo siguiente:");
            paragraph.setAlignment(Element.ALIGN_JUSTIFIED);
            paragraph.setSpacingAfter(40f);
            document.add(paragraph);

            paragraph = new Paragraph("Que por medio del presente escrito vengo en nombre y representación del hoy quejoso ya mencionado, a solicitar el AMPARO Y PROTECCIÓN DE LA JUSTICIA DE LA UNIÓN, en contra de las autoridades y actos que a continuación señalo, por lo que ajustándome al artículo 108 y 15 de la Ley de Amparo que establece lo siguiente:");
            paragraph.setAlignment(Element.ALIGN_JUSTIFIED);
            document.add(paragraph);


            paragraph = new Paragraph("“Artículo 15. Cuando se trate de actos que importen peligro de privación de la vida, ataques a la libertad personal fuera de procedimiento, incomunicación, deportación o expulsión, proscripción o destierro, extradición, desaparición forzada de personas o alguno de los prohibidos por el artículo 22 de la Constitución Política de los Estados Unidos Mexicanos, así como la incorporación forzosa al Ejército, Armada o Fuerza Aérea nacionales, y el agraviado se encuentre imposibilitado para promover el amparo, podrá hacerlo cualquiera otra persona en su nombre, aunque sea menor de edad.\n" +
                    "En estos casos, el órgano jurisdiccional de amparo decretará la suspensión de los actos reclamados, y dictará todas las medidas necesarias para lograr la comparecencia del agraviado. \n" +
                    "Una vez lograda la comparecencia, se requerirá al agraviado para que dentro del término de tres días ratifique la demanda de amparo. Si éste la ratifica por sí o por medio de su representante se tramitará el juicio; de lo contrario se tendrá por no presentada la demanda y quedarán sin efecto las providencias dictadas. \n" +
                    "Si a pesar de las medidas tomadas por el órgano jurisdiccional de amparo no se logra la comparecencia del agraviado, resolverá la suspensión definitiva, ordenará suspender el procedimiento en lo principal y se harán los hechos del conocimiento del Ministerio Público de la Federación. En caso de que éste sea autoridad responsable, se hará del conocimiento al Procurador General de la República. Cuando haya solicitud expresa de la Comisión Nacional de los Derechos Humanos, se remitirá copia certificada de lo actuado en estos casos. \n" +
                    "Transcurrido un año sin que nadie se apersone en el juicio, se tendrá por no interpuesta la demanda.“");
            paragraph.setAlignment(Element.ALIGN_JUSTIFIED);
            paragraph.setIndentationLeft(20f);
            document.add(paragraph);

            paragraph = new Paragraph("I-  NOMBRE Y DOMICILIO DEL QUEJOSO:");
            paragraph.setAlignment(Element.ALIGN_JUSTIFIED);
            paragraph.setIndentationLeft(20f);
            paragraph.setSpacingAfter(5f);
            document.add(paragraph);

            paragraph = new Paragraph("El nombre del quejoso ha quedado mencionado, pero de quien el suscrito es promovente ignora el domicilio");
            paragraph.setAlignment(Element.ALIGN_JUSTIFIED);
            paragraph.setIndentationLeft(20f);
            paragraph.setSpacingAfter(5f);
            document.add(paragraph);

            paragraph = new Paragraph("II- NOMBRE: Y DOMICILIO DEL TERCERO PERJUDICADO:");
            paragraph.setAlignment(Element.ALIGN_JUSTIFIED);
            paragraph.setIndentationLeft(20f);
            paragraph.setSpacingAfter(5f);
            document.add(paragraph);

            paragraph = new Paragraph("No existe tercero perjudicado");
            paragraph.setAlignment(Element.ALIGN_JUSTIFIED);
            paragraph.setIndentationLeft(20f);
            paragraph.setSpacingAfter(5f);
            document.add(paragraph);

            paragraph = new Paragraph("II- NOMBRE: Y DOMICILIO DEL TERCERO PERJUDICADO:");
            paragraph.setAlignment(Element.ALIGN_JUSTIFIED);
            paragraph.setIndentationLeft(20f);
            paragraph.setSpacingAfter(5f);
            document.add(paragraph);

            paragraph = new Paragraph("III- AUTORIDADES RESPONSABLES:");
            paragraph.setAlignment(Element.ALIGN_JUSTIFIED);
            paragraph.setIndentationLeft(20f);
            paragraph.setSpacingAfter(5f);
            document.add(paragraph);

            paragraph = new Paragraph("C. SECRETARIO DE SEGURIDAD CIUDADANA DE LA CIUDAD DE MÉXICO, con domicilio conocido en esta ciudad capital.\n" +
                    "C.DIRECTOR DEL CENTRO DE SANCIONES ADMINISTRATIVAS Y DE INTEGRACIÓN SOCIAL DE LA CIUDAD DE MÉXICO (TORITO)\n" +
                    "Ubicado en Avenida Aquiles Serdán, esquina con Lago Gascasónica, Colonia San Diego Ocoyoacac, C.P. 11290 de esta Ciudad, lugar en que se encuentra detenido actualmente el promovente.");
            paragraph.setAlignment(Element.ALIGN_JUSTIFIED);
            paragraph.setIndentationLeft(20f);
            paragraph.setSpacingAfter(5f);
            document.add(paragraph);

            paragraph = new Paragraph("IV- ACTO RECLAMADO:");
            paragraph.setAlignment(Element.ALIGN_JUSTIFIED);
            paragraph.setIndentationLeft(20f);
            paragraph.setSpacingAfter(5f);
            document.add(paragraph);

            paragraph = new Paragraph("ARRESTO ADMINISTRATIVO YA EJECUTÁNDOSE EN CONTRA DEL QUEJOSO EN EL CENTRO DE SANCIONES ADMINISTRATIVAS Y DE INTEGRACION SOCIAL DE ESTA CIUDAD (EL TORITO).");
            paragraph.setAlignment(Element.ALIGN_JUSTIFIED);
            paragraph.setIndentationLeft(20f);
            paragraph.setSpacingAfter(5f);
            document.add(paragraph);

            paragraph = new Paragraph("IV- - DERECHOS FUNDAMENTALES VIOLADOS.");
            paragraph.setAlignment(Element.ALIGN_JUSTIFIED);
            paragraph.setIndentationLeft(20f);
            paragraph.setSpacingAfter(5f);
            document.add(paragraph);

            paragraph = new Paragraph("Las Autoridades Orden adoras Violan en perjuicio del quejoso, los derechos fundamentales de libertad y seguridad jurídica contenidos en los artículos 14 y 16 de la Constitución Federal.");
            paragraph.setAlignment(Element.ALIGN_JUSTIFIED);
            paragraph.setIndentationLeft(20f);
            paragraph.setSpacingAfter(5f);
            document.add(paragraph);

            paragraph = new Paragraph("IV.- HECHOS.");
            paragraph.setAlignment(Element.ALIGN_JUSTIFIED);
            paragraph.setIndentationLeft(20f);
            paragraph.setSpacingAfter(5f);
            document.add(paragraph);

            paragraph = new Paragraph("Manifiesto que el día de hoy recibí una llamada telefónica del hoy quejoso, quien me manifestó lo siguiente: Que, al dirigirse a su domicilio dentro de la Ciudad de México, fue detenido ILEGALMENTE, por elementos de la Secretaria de Seguridad Ciudadana de la Ciudad de México e inmediatamente trasladado en calidad de Arrestado al Centro de Sanciones Administrativas y de Integración Social de la Ciudad de México (EL TORITO), ubicado en Avenida Aquiles Serdán, Esquina con Lago Gascasónica, Colonia San Diego Ocoyoacac, Delegación Miguel Hidalgo, C.P. 11290 de esta Ciudad, donde se encuentra ARRESTADO ILEGALMENTE.");
            paragraph.setAlignment(Element.ALIGN_JUSTIFIED);
            paragraph.setIndentationLeft(20f);
            paragraph.setSpacingAfter(5f);
            document.add(paragraph);

            paragraph = new Paragraph("CONCEPTOS DE VIOLACIÓN");
            paragraph.setAlignment(Element.ALIGN_JUSTIFIED);
            paragraph.setIndentationLeft(30f);
            paragraph.setSpacingAfter(5f);
            document.add(paragraph);

            paragraph = new Paragraph("PRIMERO. Las autoridades responsables, violan en su perjuicio los fundamentales de seguridad y legalidad, la de debida fundamentación y motivación de todo acto de autoridad, en especial las referentes a las formalidades Esenciales del Procedimiento, ya que al dictar una orden de arresto sin antes agotar las medidas de apremio que contienen el articulo 31 el Código de Procedimientos Penales para el Distrito Federal, lo anterior en virtud de que conforme al artículo 18 de la Ley de Justicia Cívica, es aplicable para los efectos del Procedimiento de Justicia Cívica y Supletoriamente lo dispuesto en ese Código, por lo que se debió primeramente apercibir con una multa, ya que al tenerlo privado de su libertad con un arresto inconmutable, se viola el artículo 14 Constitucional de nuestra carta magna.");
            paragraph.setAlignment(Element.ALIGN_JUSTIFIED);
            paragraph.setIndentationLeft(30f);
            paragraph.setSpacingAfter(5f);
            document.add(paragraph);

            paragraph = new Paragraph("Artículo 14 Constitucional, refiere en lo conducente al cumplimiento de las formalidades esenciales del Procedimiento, lo siguiente:");
            paragraph.setAlignment(Element.ALIGN_JUSTIFIED);
            paragraph.setIndentationLeft(30f);
            paragraph.setSpacingAfter(5f);
            document.add(paragraph);

            paragraph = new Paragraph("“NADIE PUEDE SER PRIVADO DE LA VIDA, DE LA LIBERTAD, O DE SUS PROPIEDADES, POSESIONES O DERECHOS, SINO MEDIANTE JUICIO SEGUIDO ANTE LOS TRIBUNALES PREVIAMENTE ESTABLECIDOS, EN EL QUE SE CUMPLAN LAS FORMALIDADES ESENCIALES DEL PROCEDIMIENTO Y CONFORME A LAS LEYES EXPEDIDAS CON ANTERIORIDAD AL HECHO.”");
            paragraph.setIndentationLeft(38f);
            paragraph.setSpacingAfter(5f);
            document.add(paragraph);

            paragraph = new Paragraph("En efecto, el derecho fundamental de legalidad y debido proceso consiste en que se debe de otorgar a todo gobernado la oportunidad de tener derecho a una defensa previa al acto que tiende a privado de su libertad, en este caso mediante la imposición de un ARRESTO INCONMUTABLE, tales derechos entre otros son los siguientes:");
            paragraph.setIndentationLeft(30f);
            paragraph.setSpacingAfter(5f);
            document.add(paragraph);

            paragraph = new Paragraph("LA NOTIFICACIÓN PERSONAL DEL INICIO DE UN PROCEDIMIENTO Y SUS CONSECUENCIAS;\n" +
                    "LA OPORTUNIDAD DE SER REPRESENTADO POR ABOGADO O PERSONA DE CONFIANZA;\n" +
                    "OFRECER Y DESAHOGAR PRUEBAS;\n" +
                    "LA OPORTUNIDAD DE PRESENTAR ALEGATOS Y\n" +
                    "EL DICTADO DE UNA RESOLUCIÓN EN QUE SE DIRIMAN LAS CUESTIONES\n");
            paragraph.setIndentationLeft(30f);
            paragraph.setSpacingAfter(5f);
            document.add(paragraph);

            paragraph = new Paragraph("Por lo que en este caso, no se cumplieron con esas formalidades esenciales del procedimiento, y por lo tardo dicho arresto es ilegal");
            paragraph.setIndentationLeft(30f);
            paragraph.setSpacingAfter(5f);
            document.add(paragraph);

            paragraph = new Paragraph("SEGUNDO: El artículo 16 Constitucional establece, en lo relativo a las condiciones mínimas para el libramiento de órdenes o mandatos que se traduzcan en la privación de la libertad, en este caso la imposición de un arresto inconmutable, lo Siguiente:");
            paragraph.setIndentationLeft(30f);
            paragraph.setSpacingAfter(5f);
            document.add(paragraph);

            paragraph = new Paragraph("“Nadie puede molestado en su PERSONA, familia, domicilio, papeles o posesiones, sino en virtud de Mandamiento escrito de la autoridad competente, que FUNDE y MOTIVE la causa legal del procedimiento.”\n" +
                    "En ese orden de ideas, se viola en perjuicio del quejoso y por parte de las autoridades responsables, lo dispuesto por el artículo en comento, al no haber hecho dichas autoridades un uso gradual de esa medidas, y por lo tanto, al privado de su libertad mediante un ARRESTO INCONMUTABLE, violando lo dispuesto por el artículo 16 Constitucional, ya que la aplicación del arresto como una SANCION ADMINISTRATIVA no se encuentra fundada y motivada, aunado • a que esa medida fue aplicada como un sanción administrativa, y por lo tanto se entiende como una medida de apremio o disciplinada, pero sin la más mínima equidad y sin ningún respeto a la libertad personal.\n" +
                    "Por lo atento es patente la violación que se comete en contra del quejoso, con el dictado y ejecución de los actos reclamados, pues éstos se producen, en franca contravención a la normatividad aplicable.\n");
            paragraph.setSpacingAfter(5f);
            document.add(paragraph);

            paragraph = new Paragraph("SOLICITUD DE SUPLENCIA DE LA QUEJA.");
            paragraph.setIndentationLeft(30f);
            paragraph.setSpacingAfter(5f);
            document.add(paragraph);

            paragraph = new Paragraph("Solicitando que, de conformidad a lo dispuesto en el artículo 79 de la Ley de Amparo, en el caso de que exista cualquier deficiencia en la presente demanda de amparo, se supla por este  H. Juez de Distrito, en atención al criterio Jurisprudencial y cuyo rubro es el siguiente: \"ARRESTO COMO MEDIDA DE APREMIO. PROCEDE LA SUPLENCIA DE LA QUEJA DEFICIENTE CUANDO SE RECLAMA EN AMPARO\"\n.");
            paragraph.setIndentationLeft(30f);
            paragraph.setSpacingAfter(5f);
            document.add(paragraph);

            paragraph = new Paragraph("SUSPENSION DE OFICIO DEL ACTO RECLAMADO.\n" +
                    "SOLICITO A NOMBRE DEL HOY QUEJOSO LA SUSPENSION DE PLANO DEL ACTO RECLAMADO, que consiste en el ataques a la libertad personal del mismo, y fuera del procedimiento judicial, la suspensión deberá concederse con fundamento en lo dispuesto por los Artículos 6, 126,127 Fr. II, 162 y demás relativos, así como aplicables de la Ley de Amparo, girando atento oficio a la autoridad que lo mantiene privado de su libertad para que le otorgue su inmediata libertad, para que la presente demanda de amparo no se quede\n" +
                    "sin materia y se sobresea.\n" +
                    "Solicitando que la suspensión de plano de los actos reclamados se otorgue en FORMA PRONTA Y EXPEDITA  a efecto de dar cumplimiento a lo dispuesto por el artículo 17 Constitucional y con el fin de que el acto reclamado no se quede sin materia\n" +
                    "Por lo anteriormente expuesto y fundado.\n" +
                    "A USTED C. JUEZ DE DISTRITO, atentamente pido:\n" +
                    "PRIMERO: Tenerme por presentado en los términos de la presente demanda de amparo, solicitando que sea admitida de Inmediato por tratarse de caso de urgencia al estar amenazada la libertad personal de la parte quejosa.\n" +
                    "SEGUNDO: Otorgar la SUSPENSION DE PLANO DE LOS ACTOS RECLAMADOS, que se solicita en el sentido de otorgar la libertad inmediata al quejoso, quien se encuentra actualmente privado de su libertad y arrestado en el Centro de Sanciones Administrativas de la Ciudad de México (El Torito) y en su oportunidad se le conceda el Amparo y la Protección de la Justicia de la Unión.\n" +
                    "PROTESTO LO NECESARIO en la fecha citada al rubro\n");
            paragraph.setIndentationLeft(30f);
            paragraph.setSpacingAfter(5f);
            document.add(paragraph);


            document.close();

            orderId= orderId.replaceAll("[^A-Za-z0-9]", "");
            Map<String, Object> userMap = new HashMap<>();
            userMap.put(orderId, new dev.com.toreritoapp.model.Amparo(datestring,"ciudad de mexico",0,"Pendiente"));
            mFirebaseDatabase.updateChildren(userMap);

            userRef = rootreference.child(idgoogle + "/"+filePath.getName());
            UploadTask uploadTask= userRef.putFile(Uri.fromFile(filePath));
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.e(Tag, "Storage"+ "error el subir al archivo");
                }
            })
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            Log.d(Tag, "Storage"+ "Archivo subido");
                        }
                    });

        } catch (Exception e) {
            Log.e("main", "error "+e.toString());
            Toast.makeText(this, "Something wrong: " + e.toString(),  Toast.LENGTH_LONG).show();
        }





    }


}
